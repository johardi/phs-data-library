import os
import re
import textwrap
from phs.data import constants
from phs.data.query import _QueryJob
from datetime import date

class Optum:
    """Build a cohort using the Optum dataset."""
    
    # Project ID
    _project_id = 'stanfordphs'
    
    # Dataset IDs
    _dataset_id = 'optum_zip5'
    
    # Table IDs
    _member_table_id = f'{_project_id}.{_dataset_id}.member'
    _claims_table_id = f'{_project_id}.{_dataset_id}.medical_claims'
    _diagnosis_table_id = f'{_project_id}.{_dataset_id}.medical_diagnosis'

    _OBSERVATION_PERIOD = """
        observation_period AS (
            SELECT t0.Patid AS person_id,
                   MIN(t0.Eligeff) AS observation_period_start_date,
                   MAX(t0.Eligend) AS observation_period_end_date,
                   t0.zip AS zip
            FROM (
                SELECT DISTINCT
                    t1.Patid,
                    t1.Eligeff,
                    t1.Eligend,
                    t1.Product,
                    CAST(CASE
                        WHEN (STRPOS(t1.Zipcode_5, '_') = 0) THEN t1.Zipcode_5
                            ELSE SUBSTR(t1.Zipcode_5, 1, STRPOS(t1.Zipcode_5, '_') - 1)
                        END AS INT64) AS zip
                FROM 
                    {person_table} AS t1
            ) AS t0
            GROUP BY t0.Patid, t0.zip, t0.Product
        )"""
    
    _PERSON = """
        person AS (
            SELECT *
            FROM (
                SELECT DISTINCT
                    t0.Patid AS person_id,
                    t0.Gdr_Cd AS gender_source_value,
                    t0.Yrdob AS year_of_birth,
                    CAST(CASE
                        WHEN (STRPOS(t0.Zipcode_5, '_') = 0) THEN t0.Zipcode_5
                            ELSE SUBSTR(t0.Zipcode_5, 1, STRPOS(t0.Zipcode_5, '_') - 1)
                        END AS INT64) AS zip
                FROM
                    {person_table} AS t0
            )
            WHERE zip in ({zipcode_valueset})
        )"""
    
    _CONDITION_OCCURRENCE = """
        condition_occurrence AS (
            SELECT DISTINCT
                t0.Patid AS person_id,
                t0.Clmid AS claim_id,
                t0.Diag AS condition_source_value,
                t1.condition_start_date
            FROM {condition_occurrence_table} AS t0
            JOIN (
                SELECT
                    t2.Patid,
                    t2.Clmid,
                    MIN(t2.Fst_Dt) AS condition_start_date
                FROM {claims_table} AS t2
                GROUP BY t2.Patid, t2.Clmid
            ) AS t1
            ON t0.Patid = t1.Patid AND t0.Clmid = t1.Clmid AND t0.Icd_Flag = 9
        )"""
    
    # SQL template for getting an initial population based on entry criteria
    _ENTRY_CRITERIA = """
        qualifying_event AS (
            SELECT DISTINCT
                p.person_id,
                p.zip,
                p.condition_start_date as index_date,
                p.observation_period_start_date,
                p.observation_period_end_date
            FROM (
                SELECT op.person_id, op.zip, op.observation_period_start_date, op.observation_period_end_date, e.condition_start_date
                FROM observation_period AS op
                JOIN (
                    SELECT p.person_id, p.zip, c.condition_start_date
                    FROM person AS p
                    JOIN (
                        SELECT *, row_number() OVER (PARTITION BY person_id ORDER BY condition_start_date) AS ordinal
                        FROM condition_occurrence
                        {condition_filter}
                    ) AS c
                    ON p.person_id = c.person_id
                    {condition_occurrence_filters}
                ) AS e
                ON op.person_id = e.person_id AND op.zip = e.zip
                    AND e.condition_start_date >= op.observation_period_start_date
                    AND e.condition_start_date <= op.observation_period_end_date
                {continuous_observation_filters}
            ) p
        )"""

    # SQL template for getting a population based on inclusion criteria
    _INCLUSION_CRITERIA = """
        inclusion_event_{event_index} AS (
            SELECT
                {event_index} as inclusion_rule_id,
                ac.person_id as person_id,
                ac.zip as zip
            FROM (
                {inclusion_criteria}
            ) AS ac
        )"""
    
    _INCLUSION_CRITERIA_GENDER = """
        SELECT DISTINCT person_id, zip
        FROM (
            SELECT p.person_id, p.zip
            FROM qualifying_event AS p
            JOIN (
                SELECT *
                FROM person
                {gender_filter}
            ) AS e
            ON p.person_id=e.person_id
        )"""


    _INCLUSION_CRITERIA_AGE = """
        SELECT DISTINCT person_id, zip
        FROM (
            SELECT p.person_id, p.zip
            FROM qualifying_event AS p
            JOIN person AS e
            ON p.person_id=e.person_id AND p.zip=e.zip
            {age_filter}
        )"""
        
    _INCLUSION_CRITERIA_CONDITION = """
        SELECT DISTINCT person_id, zip
        FROM (
            SELECT p.person_id, p.zip
            FROM qualifying_event AS p
            JOIN (
                SELECT pe.person_id, pe.zip, c.condition_start_date
                FROM person AS pe 
                JOIN (
                    SELECT *
                    FROM condition_occurrence
                    {condition_filter}
                ) AS c
                ON pe.person_id = c.person_id
                {condition_occurrence_filters}
            ) AS e
            ON p.person_id = e.person_id AND p.zip = e.zip
                AND e.condition_start_date >= p.observation_period_start_date
                AND e.condition_start_date <= p.observation_period_end_date
                {condition_occurrence_before_index_date}
                {condition_occurrence_after_index_date}
        )"""

    
    _INCLUSION_EVENT = """
        inclusion_event AS (
            SELECT inclusion_rule_id, person_id, zip
            FROM (
                {union_inclusion_criteria}
            )
        )"""

    _GET_COHORT_POPULATIONS = """
        WITH {observation_period}, {person}, {condition_occurrence}, {qualifying_event}, {inclusion_criteria}, {inclusion_event},
        cohort AS (
            SELECT mg.person_id,
                   mg.zip,
                   row_number() over (partition by person_id order by index_date asc) AS ordinal
            FROM (
                SELECT
                    q.person_id,
                    q.zip,
                    q.index_date,
                    SUM(COALESCE(CAST(POWER(CAST(2 as int64), i.inclusion_rule_id) AS int64), 0)) AS inclusion_rule_mask
                FROM qualifying_event q
                LEFT JOIN inclusion_event i 
                ON i.person_id = q.person_id AND i.zip = q.zip
                GROUP BY q.person_id, q.zip, q.observation_period_start_date, q.observation_period_end_date, q.index_date
            ) mg
            WHERE (mg.inclusion_rule_mask = POWER(CAST(2 AS int64), {inclusion_criteria_count}) - 1)
        )
        SELECT person_id, zip
        FROM cohort
        WHERE ordinal = 1
        ORDER BY zip"""
    
    def __init__(self, useSample=True): 
        if useSample:
            self._member_table_id = f'{self._project_id}.{self._dataset_id}:sample.member'
            self._claims_table_id = f'{self._project_id}.{self._dataset_id}:sample.medical_claims'
            self._diagnosis_table_id = f'{self._project_id}.{self._dataset_id}:sample.medical_diagnosis'
        
        
    def info(self):
        """Returns a brief description about the dataset.
        
        Returns:
            A description string about the dataset.
        """
        print("Optum ZIP5 Dataset v7.1")
    

    def build_cohort_definition(self, zips, entry_criteria, inclusion_criteria=None, exit_criteria=None):
        """Build a cohort definition.
        
        This cohort is built based on a list of entry criteria, inclusion criteria and exit
        criteria within the given ZIP codes. 
        
        Args:
            zips (list): A list of US ZIP codes
            entry_criteria (dict of str: dict): The cohort entry criteria defines the
                conditions when people initially enter the cohort. The set of people having
                an entry event is referred to as the initial event cohort.
            inclusion_criteria(list, optional): The inclusion criteria defines a further
                restriction to the initial event cohort. Each inclusion criterion will be
                evaluated to determine the impact of the criteria on the attrition of persons
                from the initial event cohort. The qualifying cohort is defined as all people
                in the initial event cohort that satisfy all inclusion criteria.
            exit_criteria(dict of str: dict, optional): The cohort exit criteria defines when
                a person no longer qualifies for cohort membership. Cohort exit can be defined
                in multiple ways such as the end of the observation period, a fixed time
                interval relative to the initial entry event, the last event in a sequence of
                related observations (e.g. persistent drug exposure) or through other
                censoring of observation period.
            
        Returns:
            A query job that contains the generated BigQuery string.
        """
        entry_criteria_sql = self._build_entry_criteria_sql(entry_criteria)
        if inclusion_criteria:
            inclusion_criteria_sql = self._build_inclusion_criteria_sql(inclusion_criteria)
            inclusion_event_sql = self._build_inclusion_event_sql(inclusion_criteria)
        if exit_criteria:
            exit_criteria_sql = self._build_exit_criteria_sql(exit_criteria)
 
        query = textwrap.dedent(self._GET_COHORT_POPULATIONS.format(
                observation_period=self._OBSERVATION_PERIOD.format(
                        person_table=self._member_table_id),
                person=self._PERSON.format(
                        person_table=self._member_table_id,
                        zipcode_valueset=",".join(str(z) for z in zips)),
                condition_occurrence=self._CONDITION_OCCURRENCE.format(
                        condition_occurrence_table=self._diagnosis_table_id,
                        claims_table=self._claims_table_id),
                qualifying_event=entry_criteria_sql,
                inclusion_criteria=inclusion_criteria_sql,
                inclusion_event=inclusion_event_sql,
                inclusion_criteria_count=len(inclusion_criteria)))
        
        # Execute the query
        return _QueryJob(query)

    def _build_entry_criteria_sql(self, entry_criteria):
        """Build query string for the entry criteria."""
        entry_criteria_sql = ""
        for k, v in entry_criteria.items():
            if k == 'event':
                event_type = list(v.keys())[0]
                if event_type == 'condition':
                    params = self._get_condition_occurrence_parameters(entry_criteria)
                    entry_criteria_sql = self._build_condition_entry_criteria_sql(params)
        return entry_criteria_sql
    
    def _get_condition_occurrence_parameters(self, criteria):
        """Rebuild internally the condition occurrence parameters."""
        condition_occurrence_parameters = {}
        condition_occurrence_parameters['condition_filter'] = ""
        condition_occurrence_parameters['condition_occurrence_filter'] = ""
        condition_occurrence_parameters['condition_restriction_before_index_date_filter'] = ""
        condition_occurrence_parameters['condition_restriction_after_index_date_filter'] = \
                "AND e.condition_start_date <= DATE_ADD(CAST(p.index_date AS date), INTERVAL 0 DAY)"
        condition_occurrence_parameters['continuous_observation_filter'] = ""
        for k, v in criteria.items():
            if k == 'event':
                condition_value = v['condition']
                if type(condition_value) is str:
                    codes = constants._CONDITIONS[condition_value]['codes']
                    condition_valueset = ",".join(f'"{x}"' for x in codes)
                    condition_occurrence_parameters['condition_filter'] = \
                            f'WHERE condition_source_value IN ({condition_valueset})'
                elif type(condition_value) is list:
                    condition_valueset = ",".join(f'"{x}"' for x in condition_value)
                    condition_occurrence_parameters['condition_filter'] = \
                            f'WHERE condition_source_value IN ({condition_valueset})'
            elif k == 'occurrence':
                occurrence_type = list(v.keys())[0]
                if occurrence_type == 'startDateBefore':
                    occurrence_date = v['startDateBefore']
                    condition_occurrence_parameters['condition_occurrence_filter'] = \
                            f'WHERE c.ordinal = 1 AND c.condition_start_date < "{occurrence_date}"'
                elif occurrence_type == 'startDateAfter':
                    occurrence_date = v['startDateAfter']
                    condition_occurrence_parameters['condition_occurrence_filter'] = \
                            f'WHERE c.ordinal = 1 AND c.condition_start_date > "{occurrence_date}"'
                elif occurrence_type == 'startDateAt':
                    occurrence_date = v['startDateAt']
                    condition_occurrence_parameters['condition_occurrence_filter'] = \
                            f'WHERE c.ordinal = 1 AND c.condition_start_date = "{occurrence_date}"'
                elif occurrence_type == 'startDateBetween':
                    occurrence_start_date = v['startDateBetween'].split('/')[0]
                    occurrence_end_date = v['startDateBetween'].split('/')[1]
                    condition_occurrence_parameters['condition_occurrence_filter'] = \
                            f'WHERE c.ordinal = 1 AND c.condition_start_date >= "{occurrence_start_date}" ' \
                            f'AND c.condition_start_date <= "{occurrence_end_date}"'
            elif k == 'restriction':
                for restriction_type, restriction_value in v.items():
                    if restriction_type == 'withinDaysBeforeIndexDate':
                        condition_occurrence_parameters['condition_restriction_before_index_date_filter'] = \
                                f'AND e.condition_start_date >= DATE_ADD(CAST(p.index_date AS date), ' \
                                f'INTERVAL -{restriction_value} DAY)'
                    elif restriction_type == 'withinDaysAfterIndexDate':
                        condition_occurrence_parameters['condition_restriction_after_index_date_filter'] = \
                                f'AND e.condition_start_date <= DATE_ADD(CAST(p.index_date AS date), ' \
                                f'INTERVAL {restriction_value} DAY)'
            elif k == 'continuousObservation':
                days_before = v['minDaysBeforeIndexDate'] if 'minDaysBeforeIndexDate' in v else 0
                days_after = v['minDaysAfterIndexDate'] if 'minDaysAfterEntryEvent' in v else 0
                condition_occurrence_parameters['continuous_observation_filter'] = \
                        f'WHERE DATE_ADD(op.observation_period_start_date, ' \
                        f'INTERVAL {days_before} DAY) <= e.condition_start_date ' \
                        f'AND DATE_ADD(e.condition_start_date, ' \
                        f'INTERVAL {days_after} DAY) <= op.observation_period_end_date'         
        return condition_occurrence_parameters
    
    def _build_condition_entry_criteria_sql(self, condition_occurrence_parameters):
        """Build query string for the entry criteria based on the condition occurrence parameters."""
        return textwrap.dedent(self._ENTRY_CRITERIA.format(
                condition_filter=condition_occurrence_parameters['condition_filter'],
                condition_occurrence_filters=condition_occurrence_parameters['condition_occurrence_filter'],
                continuous_observation_filters=condition_occurrence_parameters['continuous_observation_filter']))
    
    def _build_inclusion_criteria_sql(self, inclusion_criteria):
        """Build query string for the inclusion criteria."""
        inclusion_criteria_list = []
        for index, inclusion_criterion in enumerate(inclusion_criteria, start=0):
            for k, v in inclusion_criterion.items():
                if k == 'gender':
                    gender_code = constants._GENDERS[v]['value']
                    filter_sql = f'WHERE gender_source_value = "{gender_code}"'
                    inclusion_criteria_sql = self._build_gender_inclusion_criteria_sql(index, filter_sql)
                    inclusion_criteria_list.append(inclusion_criteria_sql)
                elif k == 'ageGroup':
                    start_age = constants._AGE_GROUPS[v]['start']
                    end_age = constants._AGE_GROUPS[v]['end']
                    filter_sql = f'WHERE EXTRACT(YEAR FROM index_date)-year_of_birth >= {start_age} ' \
                            f'AND EXTRACT(YEAR FROM index_date)-year_of_birth <= {end_age}'
                    inclusion_criteria_sql = self._build_age_inclusion_criteria_sql(index, filter_sql)
                    inclusion_criteria_list.append(inclusion_criteria_sql)
                elif k == 'ageRange':
                    if not re.match(r"[0-9]*-[0-9]*", v):
                        raise ValueError("Unable to parse value range expression. Some valid examples: -22, or 16-22, or 22-")
                    start_age = int(v.split('-')[0]) if v.split('-')[0] != '' else 0
                    end_age = int(v.split('-')[1]) if v.split('-')[1] != '' else 999
                    filter_sql = f'WHERE EXTRACT(YEAR FROM index_date)-year_of_birth >= {start_age} ' \
                            f'AND EXTRACT(YEAR FROM index_date)-year_of_birth <= {end_age}'
                    inclusion_criteria_sql = self._build_age_inclusion_criteria_sql(index, filter_sql)
                    inclusion_criteria_list.append(inclusion_criteria_sql)
                elif k == 'ageAt':
                    age_value = v
                    filter_sql = f'WHERE EXTRACT(YEAR FROM index_date)-year_of_birth = {age_value}'
                    inclusion_criteria_sql = self._build_age_inclusion_criteria_sql(index, filter_sql)
                    inclusion_criteria_list.append(inclusion_criteria_sql)
                elif k == 'event':
                    event_type = list(v.keys())[0]
                    if event_type == 'condition':
                        condition_occurrence_parameters = self._get_condition_occurrence_parameters(inclusion_criterion)
                        inclusion_criteria_sql = self._build_condition_inclusion_criteria_sql(index, condition_occurrence_parameters)
                        inclusion_criteria_list.append(inclusion_criteria_sql)
        return ",".join(ic for ic in inclusion_criteria_list)

    def _build_gender_inclusion_criteria_sql(self, index, gender_filter_sql):
        """Build query string for the inclusion criteria based on the gender parameters."""
        inclusion_criteria_sql = self._INCLUSION_CRITERIA_GENDER.format(gender_filter=gender_filter_sql)
        return self._INCLUSION_CRITERIA.format(
                event_index=index,
                inclusion_criteria=inclusion_criteria_sql)

    def _build_age_inclusion_criteria_sql(self, index, age_filter_sql):
        """Build query string for the inclusion criteria based on the age parameters."""
        inclusion_criteria_sql = self._INCLUSION_CRITERIA_AGE.format(age_filter=age_filter_sql)
        return self._INCLUSION_CRITERIA.format(
                event_index=index,
                inclusion_criteria=inclusion_criteria_sql)
    
    def _build_condition_inclusion_criteria_sql(self, index, condition_occurrence_parameters):
        """Build query string for the inclusion criteria based on the condition occurrence parameters."""
        inclusion_criteria_sql = self._INCLUSION_CRITERIA_CONDITION.format(
                condition_filter=condition_occurrence_parameters['condition_filter'],
                condition_occurrence_filters=condition_occurrence_parameters['condition_occurrence_filter'],
                condition_occurrence_after_index_date=condition_occurrence_parameters['condition_restriction_after_index_date_filter'],
                condition_occurrence_before_index_date=condition_occurrence_parameters['condition_restriction_before_index_date_filter'])
        return self._INCLUSION_CRITERIA.format(
                event_index=index,
                inclusion_criteria=inclusion_criteria_sql)

    def _build_inclusion_event_sql(self, inclusion_criteria):
        """Build query string for the inclusion event based on the inclusion criteria."""
        ic_size = len(inclusion_criteria)
        inclusion_criteria_members = [f'SELECT inclusion_rule_id, person_id, zip FROM inclusion_event_{i}' for i in range(ic_size)]
        return self._INCLUSION_EVENT.format(
                union_inclusion_criteria="\nUNION ALL\n".join(member_sql for member_sql in inclusion_criteria_members))