_GENDERS = {
    'Male': {
        'name': 'Gender Male',
        'value': 'M'
    },
    'Female': {
        'name': 'Gender Female',
        'value': 'F'
    }
}

_AGE_GROUPS = {
    'Under5Years': {
        'name': 'Under 5 years',
        'start': 0,
        'end': 4,
        'unitOfMeasurement': 'Year'
    },
    'Under18Years': {
        'name': 'Under 18 years',
        'start': 0,
        'end': 17,
        'unitOfMeasurement': 'Year'
    },
    'Years5To17': {
        'name': '5 to 17 years',
        'start': 5,
        'end': 17,
        'unitOfMeasurement': 'Year'
    },
    'Years18To24': {
        'name': '18 to 24 years',
        'start': 18,
        'end': 24,
        'unitOfMeasurement': 'Year'
    },
    'Years18To44': {
        'name': '18 to 44 years',
        'start': 18,
        'end': 44,
        'unitOfMeasurement': 'Year'
    },
    'Years25To44': {
        'name': '25 to 44 years',
        'start': 25,
        'end': 44,
        'unitOfMeasurement': 'Year'
    },
    'Years45To64': {
        'name': '45 to 64 years',
        'start': 45,
        'end': 64,
        'unitOfMeasurement': 'Year'
    },
    'Above65Years': {
        'name': 'Above 65 years',
        'start': 65,
        'end': 999,
        'unitOfMeasurement': 'Year'
    }
}
    
_CONDITIONS = {
    'Asthma': {
        'name': 'Asthma',
        'vocabulary': 'ICD9',
        'codes': ( "49300", "49301", "49302", "49310", "49311", "49312", "49320", "49321", "49322", "49382", "49390", "49391", "49392" )
    },
    'Heart Disease': {
        'name': 'Heart disease',
        'vocabulary': 'ICD9',
        'codes': ( "42900", "42910", "42920", "42930", "42940", "42950", "42960", "42970", "42971", "42979", "42980", "42981", "42982", "42983", "42989", "42990" )
    },
    'Lung Disease': {
        'name': 'Lung disease',
        'vocabulary': 'ICD9',
        'codes': ( "51800", "51810", "51820", "51830", "51840", "51850", "51851", "51853", "51852", "51860", "51870", "51880", "51881", "51882", "51883", "51884", "51889" )
    }
}
