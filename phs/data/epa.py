"""

"""

import os
import re
import textwrap
import pandas as pd
from datetime import date
from dateutil.relativedelta import relativedelta
from redivis import bigquery

class PM25:
    """
        
    """
    # Project ID
    _project_id = 'stanfordphs'
    
    # Dataset IDs
    _dataset_id = 'ZipCode_lat_long'
    
#     _crosswalk_table_id = f'{_project_id}.{_dataset_id}.main'
    _crosswalk_table_id = ':47841'
    _pm25_table_id = ':43913'
    
    # Google BigQuery Client
    _client = bigquery.Client()
    
    _LOCATION = """
        location AS (
            SELECT zip,
                   city,
                   state,
                   latitude,
                   longitude
            FROM {crosswalk_table}
            WHERE zip IN ({zipcode_valueset})
        )"""
    
    _PM25_SENSOR = """
        sensor AS (
            SELECT DISTINCT
                   Site_ID AS sensor_id,
                   Site_Name AS sensor_name,
                   SITE_LATITUDE AS latitude,
                   SITE_LONGITUDE AS longitude
            FROM {pm25_table}
        )"""
    
    _PM25_READING = """
        sensor_reading AS (
            SELECT Site_ID AS sensor_id,
                   Date AS reading_date,
                   Daily_Mean_PM2_5_Concentration AS concentration,
                   DAILY_AQI_VALUE AS aqi
            FROM {pm25_table}
            WHERE Date >= "{observation_start_date}" AND Date <= "{observation_end_date}"
        )"""
    
    _GET_PM25_SENSORS = """
        WITH {location}, {sensor}
        SELECT l.zip,
               l.city,
               l.state,
               s.sensor_id,
               s.sensor_name,
               ST_DISTANCE(
                   ST_GEOGPOINT(l.longitude, l.latitude), 
                   ST_GEOGPOINT(s.longitude, s.latitude)
               ) / 1000 AS km_distance
        FROM location AS l
        JOIN sensor AS s
        ON ST_DWITHIN(
            ST_GEOGPOINT(l.longitude, l.latitude), 
            ST_GEOGPOINT(s.longitude, s.latitude),
            {meters_within})
        ORDER BY l.zip, s.sensor_id"""

    
    _GET_PM25_READINGS = """
        WITH {location}, {sensor}, {sensor_reading}
        SELECT ss.zip,
               ss.sensor_id,
               sr.reading_date,
               sr.concentration,
               sr.aqi
        FROM (
            SELECT l.zip,
                   l.city,
                   l.state,
                   s.sensor_id
            FROM location AS l
            JOIN sensor AS s
            ON ST_DWITHIN(
                ST_GEOGPOINT(l.longitude, l.latitude), 
                ST_GEOGPOINT(s.longitude, s.latitude),
                {meters_within})
        ) AS ss
        JOIN sensor_reading AS sr
        ON ss.sensor_id = sr.sensor_id
        ORDER BY ss.zip, ss.sensor_id, sr.reading_date"""
        
#     def __init__(self, useSample=True):
#         if useSample:
#             self._crosswalk_table_id = f'{self._project_id}.{self._dataset_id}:sample.main'
    
    def evaluate(self, query):
        """Returns a :obj:`pandas.DataFrame`  

        Args:

        Returns:

        """
        result = self._client.query(query)
        return result.to_dataframe()
    
    def get_sensors(self, zips, kilometers_within=10):
        meters_within = kilometers_within * 1000
        query = textwrap.dedent(
                self._GET_PM25_SENSORS.format(
                     location=self._LOCATION.format(
                         crosswalk_table=self._crosswalk_table_id,
                         zipcode_valueset=",".join(str(z) for z in zips)),
                     sensor=self._PM25_SENSOR.format(
                         pm25_table=self._pm25_table_id),
                     meters_within=meters_within))
        return self.evaluate(query)
    
    def get_sensor_readings(self, zips, kilometers_within=10, observation_start_date=None, observation_end_date=None, average_by=None):
        meters_within = kilometers_within * 1000
        if not observation_end_date:
            observation_end_date = date.today().isoformat()
        if not observation_start_date:
            observation_start_date = (date.today + relativedelta(months=-12)).isoformat()
        query = textwrap.dedent(
                self._GET_PM25_READINGS.format(
                     location=self._LOCATION.format(
                         crosswalk_table=self._crosswalk_table_id,
                         zipcode_valueset=",".join(str(z) for z in zips)),
                     sensor=self._PM25_SENSOR.format(
                         pm25_table=self._pm25_table_id),
                     sensor_reading=self._PM25_READING.format(
                         pm25_table=self._pm25_table_id,
                         observation_start_date=observation_start_date,
                         observation_end_date=observation_end_date),
                     meters_within=meters_within))
        result = self.evaluate(query)
        
        if average_by:
            result.set_index('reading_date', inplace=True)
            result.index = pd.to_datetime(result.index)
            if average_by == "weekly":
                result = result.groupby(['zip', 'sensor_id'], as_index=True) \
                               .resample('W').mean()[['concentration', 'aqi']] \
                               .reset_index()
            elif average_by == "monthly":
                result = result.groupby(['zip', 'sensor_id'], as_index=True).resample('M').mean()[['concentration', 'aqi']].reset_index(level=0, inplace=True)
            elif average_by == "yearly":
                result = result.groupby(['zip', 'sensor_id'], as_index=True).resample('Y').mean()[['concentration', 'aqi']].reset_index(level=0, inplace=True)

        return result