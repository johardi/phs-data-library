import sqlparse
from redivis import bigquery

class _QueryJob:
    
    _query = None

    # Google BigQuery Client
    _client = bigquery.Client()
    
    def __init__(self, query):
        self._query = query
    
    def evaluate(self):
        """Evaluate the query.
        
        Submit the query to BigQuery and return the results
        as a data frame.
            
        Returns:
            A data frame representing the query result
        """
        return self._client.query(self._query).to_dataframe()
        
    def show(self):
        """Get the query string"""
        return print(sqlparse.format(self._query, reindent=True))